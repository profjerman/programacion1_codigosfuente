/*
En este codigo fuente se muestra c�mo se trabaja con archivos de texto
*/
#include <stdio.h>
#define DIM 10

typedef struct {
    int legajo;
    char genero;
} registro;

void banner(){
    printf("***********************************\n");
    printf("*** Aprendiendo a usar punteros ***\n");
    printf("***********************************\n");
    printf("\n");
}

int menu(){
    int a;
    printf("****** Menu ******\n");
    printf("Elija una opcion:\n");
    printf("0- Salir del programa\n");
    printf("1- Primer uso de punteros\n");
    printf("2- Operaciones aritmeticas con valores apuntados\n");
    printf("3- Operaicones aritmeticas sobre punteros\n");
    printf("4- Punteros en estructuras\n");
    printf("5- Pidiendo memoria para punteros\n");
    scanf("%d",&a);
    return a;
}

void errores(int i){
    switch(i){
    case 1: printf("Opcion erronea\n"); break;
    default: printf("Error desconocido\n"); break;
    }
}

void f1_primerUso(){
    int c;
    int *ptr=NULL;

    c=8;
    printf("valores: c=%d ptr=%d | ",c,ptr);
    printf("direcciones: c=%x ptr=%x &ptr=%x\n",&c,ptr,&ptr);
    ptr=&c;
    printf("valores: c=%d ptr=%d | ",c,ptr);
    printf("direcciones: c=%x ptr=%x &ptr=%x\n",&c,ptr,&ptr);
    *ptr*=2;
    printf("valores: c=%d ptr=%d | ",c,ptr);
    printf("direcciones: c=%x ptr=%x &ptr=%x\n",&c,ptr,&ptr);

    printf("\n");

    /*
    IMPORTANTE:
    Los punteros son variables que guardan valores que deben ser direcciones de memoria
    Pero C no valida los valores, por lo que se le puede asignar cualquier cosa,
    como en la siguiente linea:
    */
    ptr=3500;
    /*
    Podemos mostrar el valor apuntado por una direccion decimal, pero esta fuera del rango
    de memoria del proceso, por lo cual escribir esa celda de memoria rompe el programa:
    *ptr=2;
    */

    /*
    Modifiquen esta funcion para que haga lo mismo pero mostrando:
    - el caracter M
    - un numero decimal cualquiera
    */
}

void f2_operacionesDeValoresApuntados(){
    int val, *ptr;

    val=10;
    printf("Val vale %d\n",val);
    ptr=&val;
    printf("ptr apunta a val\n");
    *ptr+=5;
    printf("Desde ptr incrementamos val, ahora val vale %d\n",val);
    printf("Y valen todas las operaciones artmeticas y logicas para los valores apuntados:\n");
    printf("%d - %d - %d\n",*ptr+4,*ptr-4,*ptr*4);
}

void f3_operacioonesSobrePunteros(){
    int i, vec[DIM], *ptr;

    for (i=0; i<DIM; i++)
        vec[i]=(i+1)*3;
    printf("El vector esta cargado con 'la tabla del 3':\n");
    for (i=0; i<DIM; i++)
        printf("%d ",vec[i]);
    printf("\n");
    ptr=vec;
    printf("ptr ahora apunta a vec (asignacion compatible porque la variable vector es una direccion de memoria que apunta al primer elemento del array)\n");

    printf("El elemento apuntado por ptr es %d\n",*ptr);
    printf("El si le sumo 3 a ptr (que es direccion de mem), obtengo %d\n",*(ptr+3));
    printf("El si le sumo 1 a ptr, obtengo %d\n",*(++ptr));
    printf("Si INCREMENTO a ptr en 4, obtengo %d\n", *(ptr+=4));
    printf("Ahora ptr apunta a %d, perdi la referencia al primer elemento!\n",*ptr);
    ptr+=4;
    printf("Incremente a ptr en 4 y apunto al %d\n",*ptr);
    printf("Pero al ser puntero, puedo seguir incrementandolo y apuntar a cualquier lado\n");
    ptr++;
    printf("El 'espacio 11' del vector esta fuera de su rango, pero el puntero lo apunta y vale basura (%d)\n",*ptr);
    printf("\n");
}

void f4_punterosEnEstructuras(){
    registro unReg, *regPtr;

    unReg.genero='M';
    unReg.legajo=123456;
    printf("Registro cargado\n");
    regPtr=&unReg;
    printf("Puntero apuntando al registro\n");
    printf("Acceso al campo desde el puntero; el legajo es %d\n",(*regPtr).legajo);
    printf("Accediendo con el operador 'flecha' al campo directamente; genero:%c\n",regPtr->genero);
    printf("\n");
}

void f5_pidiendoMemoria(){
    int *unPuntero=NULL;

    printf("El puntero apunta a NULL\n");
    unPuntero=malloc(sizeof(int));
    printf("Se le asigno espacio de un int al puntero\n");
    printf("El valor que tiene donde apunta es %d (es basura)\n",*unPuntero);
    *unPuntero=2020;
    printf("Se guardo el dato %d en el puntero\n",*unPuntero);
    free(unPuntero);
    printf("Se libero el espacio de memoria asignado al puntero\n");
    printf("El valor que tiene donde apunta es %d (es basura)\n",*unPuntero);
    printf("\n");
    /*
    Para los curiosos, investiguen como pedir memoria para un vector...
    TIP: el tema se ver� en Estructuras Dinamicas, Modulo 7
    */
}

int main(){
    int opcion;

    banner();
    opcion=menu();
    while(opcion!=0){
        switch(opcion){
            case 0:
                return 0;
            case 1:
                f1_primerUso();
                break;
            case 2:
                f2_operacionesDeValoresApuntados();
                break;
            case 3:
                f3_operacioonesSobrePunteros();
                break;
            case 4:
                f4_punterosEnEstructuras();
                break;
            case 5:
                f5_pidiendoMemoria();
                break;
            default:
                errores(1);
                break;
        }
        opcion=menu();
    }
    return 0;
    }
