/*
En este codigo fuente se muestra c�mo se trabaja con vectores
*/

#include <stdio.h>
#define DIM 20

void f1_declaracion(){
    int i;
    /* asi se declara un vector */
    int vec1[10]; /* se debe especificar el tama�o siempre, el vector queda vacio */
    char vec2[] = {'a','b','c','d'}; /* no especifico tama�p pero toma el tama�o de la cantidad de elementos */
    int vec3[DIM]={11,22,33}; /* uso de valores con instrucciones DEFINE */
    /*
    carga del vector por posicion,
    est� o no iniciado
    */
    printf("Declaracion de vectores, asignacion y acceso de valores\n");
    vec1[0]=999;
    vec1[3]=888;
    vec2[2]='z';
    /*
    mostrar datos del vector
    */
    printf("Algunos datos: %d %d %c\n", vec1[0], vec1[3],vec2[2]);
}

void f2_recorriendo(){
    /* ciclo que recorre el vector */
    int i, vec1[DIM]={11,22,33,44,55,66,77,88,99};

    printf("Mostrar vector\n");
    for (i=0 ; i<DIM ; i++)
        printf("%d-%d ",i,vec1[i]);
}

void f3_promedio(){
    /* como se recorre el vector y se procesa el resultado */
    int i, vec[DIM]={11,22,22,33,32,23,55,66,77,88,99,76,45,43,67,71,2,39,44};
    float sum;

    for (i=0 ; i<DIM ; i++)
        sum += vec[i];
    printf("Promedio = %2.2f\n",sum/DIM);
}

void f4_duplicarVector(int* vec, int tam){
    /*
    El vector se pasa como un puntero, por ende hay que
    pasar el tama�o del vector como otro parametro para no
    pasarse del tama�o real del vector
    */
    int i;
    printf("Duplicando los valores del vector\n");
    for (i=0 ; i<tam ; i++)
        vec[i]*=2;
    printf("\n");
    /*
    ver que el vector, al ser una direccion de memoria,
    SIEMPRE se pasa por valor. Pero al ser una direccion de
    memoria, su contenido SIEMPRE se puede modificar dentro
    de la funcion!!!
    */
}

void f5_recorriendo2(int* vec, int tam){
    /*
    recorriendo el vector como un puntero
    */
    int i;
    printf("Recorriendo vector como puntero\n");
    for (i=0 ; i<DIM ; i++,vec++)
        printf("%d-%d ",i,*vec);
    printf("\n");
}

int menu(){
    int opcion;

    printf("*** Vectores ***\n");
    printf("1- Declaracion\n");
    printf("2- Recorriendo el vector\n");
    printf("3- Calculo del promedio\n");
    printf("4- Duplicacion del vector\n");
    printf("5- Recorriendo el vector v2\n");
    printf("6- Salir\n");
    scanf("%d",&opcion);
    return opcion;
}

int main(){
    int vec[DIM]={11,22,22,33,32,23,55,66,77,88,99,76,45,43,67,71,2,39,44},i=0;
    int opcion;

    opcion=menu();
    while (opcion!=6){
        switch(opcion){
            case 1:
                f1_declaracion();
                printf("\n\n");
                break;
            case 2:
                f2_recorriendo();
                printf("\n\n");
                break;
            case 3:
                f3_promedio();
                printf("\n\n");
                break;
            case 4:
                f4_duplicarVector(vec,DIM);
                for (i=0;i<DIM;i++)
                    printf("%d-%d ",i,vec[i]);
                printf("\nNotar que el vector se pasa siempre por valor, pero al ser una direccion de memoria su contenido siempre se puede modificar!!!\n");
                printf("\n\n");
                break;
            case 5:
                f5_recorriendo2(vec,DIM);
                printf("Recorriendo el vector normalmente\n");
                for (i=0;i<DIM;i++)
                    printf("%d-%d ",i,vec[i]);
                printf("\n\n");
                break;
            default:
                printf("Opcion invalida!\n");
                printf("\n\n");
                break;
        }
        opcion=menu();
    }

    return 0;
    }
