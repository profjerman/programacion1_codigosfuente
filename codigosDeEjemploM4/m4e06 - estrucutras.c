/*
En este codigo fuente se muestra c�mo se trabaja con estructuras
*/
#include <stdio.h>
#define DIM 4

// definicion de una estructura llamada "alumno", para usarla en todo el programa
struct alumno {
    int legajo;
    int edad;
    int nota1;
    int nota2;
    float promedio;
};

struct alumnoConNotas{
    int legajo;
    int notas[DIM];
    float promedio;
};

enum colores {
    BLANCO=1, AMARILLO, NEGRO, VERDE
};

void f1_declaracion(){
    // aqui se usa el tipo de dato "struct alumno" declarado fuera de main
    struct alumno unAlumno;

    unAlumno.legajo = 93535;
    unAlumno.edad = 23;
    unAlumno.nota1 = 7;
    unAlumno.nota2 = 4;
    unAlumno.promedio = (unAlumno.nota1 + unAlumno.nota2) / 2.0;
    printf("El legajo %d tiene un promedio es %2.2f\n",unAlumno.legajo,unAlumno.promedio);
}

void f2_declaracionDentroDeFuncion(){
    // declaracion de un tipo de dato "struct miEstructura" local a esta funcion
    struct miEstructura {
        int legajo;
        int edad;
    } alu1;

    alu1.legajo=1;
    printf("El legajo es %d\n",alu1.legajo);
    /*
    Este tipo de declaracion no se utiliza porque la estructura solamente existe
    en esta funcion. Por eso se declaran fuera del main, para que su alcance
    sea en todo el programa.
    */
}

void f3_inicializacionAvanzada(){
    // en este tipo de inicializacion es neceario asignar un valor cada campo
    // Notar que se debe respetar la posicion de cada campo.
    int i, acumNotas=0;
    struct alumno unAlumno = {93535,23,8,6,0.0};
    // atentos al detalle del vector de notas
    struct alumnoConNotas otroAlumno = {99878,{7,8,9,4},0.0};
    printf("El legajo %d tiene un promedio es %2.2f\n",unAlumno.legajo,unAlumno.promedio);

    printf("Las notas del otro alumno son: ");
    for (i=0 ; i<DIM ; i++){
        printf("%d ",otroAlumno.notas[i]);
        acumNotas += otroAlumno.notas[i];
    }
    printf("y el promedio es de %2.2f\n",acumNotas/(float)DIM);
}

void f4_asignacion(){
    struct alumnoConNotas alumno1 = {99878,{7,8,9,4},0.0};
    struct alumnoConNotas alumnoAux;

    alumnoAux = alumno1;
    printf("El legajo del alumno auxiliar es %d\n",alumnoAux.legajo);
    /*
    Esto es posible porque todos los campos de las estructuras son de tama�o fijo y
    van en el mismo orden, por lo que la asignacion es una copia campo a campo.
    O sea, se las puede tratar como variables primitivas, eso incluye el pasaje
    por referencia
    */
}

void f5_pasajePorReferencia(struct alumnoConNotas* unAlu){
    struct alumnoConNotas otroAlu = {99878,{7,8,9,4},0.0};

    // desreferenciamos la variable para luego poder acceder al campo...
    (*unAlu).legajo=otroAlu.legajo;
    (*unAlu).legajo=12345678;
    // o usamos el operador "flecha": de una direccion de memoria accedemos directamente al campo...
    unAlu->promedio=5.5;
    // arriba, ambas formas son validas para acceder a los campos desde un puntero a struct

    printf("El legajo del pasado por referencia es %d con promedio %2.2f\n", unAlu->legajo,(*unAlu).promedio);
}

void f6_estructurasEnVectores(){
    struct alumno misAlumnos[DIM];
    int i;

    printf("Cargando vector de alumnos...\n");
    for (i=0 ; i<DIM ; i++){
        misAlumnos[i].legajo = 100 + i;
    }
    printf("Los legajos cargados son:\n");
    for (i=0 ; i<DIM ; i++){
        printf("Legajo %d: %d\n",i,misAlumnos[i].legajo);
    }
}

void f7_enumeraciones(){
    enum colores colorDelCoche=AMARILLO;

    switch (colorDelCoche){
    case BLANCO:
        printf("el color es el blanco!\n"); break;
    case AMARILLO:
        printf("el color es el amarillo!\n"); break;
    default:
        printf("Color no relevante\n");
    }
    printf("El valor asociado al color es el %d\n",colorDelCoche);
}

int menu(){
    int opt;

    printf("*** Estructuras ***\n");
    printf("1- Declaracion\n");
    printf("2- Declaracion dentro de una funcion\n");
    printf("3- Inicializacion avanzada\n");
    printf("4- Asignaciones\n");
    printf("5- Pasaje por referencia\n");
    printf("6- Estructuras en vectores\n");
    printf("7- Enumeraciones\n");
    printf("0- Salir\n");
    scanf("%d",&opt);
    return opt;
}

int main(){
    int opcion;
    struct alumnoConNotas miAlumno;

    opcion=menu();

    while (opcion != 0){
        switch(opcion){
        case 1:
            f1_declaracion();
            printf("\n\n");
            break;
        case 2:
            f2_declaracionDentroDeFuncion();
            printf("\n\n");
            break;
        case 3:
            f3_inicializacionAvanzada();
            printf("\n\n");
            break;
        case 4:
            f4_asignacion();
            printf("\n\n");
            break;
        case 5:
            f5_pasajePorReferencia(&miAlumno);
            printf("Fuera de la funcion, el legajo es %d\n",miAlumno.legajo);
            printf("\n\n");
            break;
        case 6:
            f6_estructurasEnVectores();
            printf("\n\n");
            break;
        case 7:
            f7_enumeraciones();
            printf("\n\n");
            break;
        default:
            printf("*** OPCION INVALIDA ***\n\n\n");
        }
        opcion=menu();
    }
    return 0;
    }
