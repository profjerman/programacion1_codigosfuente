/*
En este codigo fuente se muestra c�mo se trabaja con estructuras
*/
#include <stdio.h>
#define DIM 4

void inicializarVector10(int* vec){
    vec[0]=6;
    vec[1]=8;
    vec[2]=5;
    vec[3]=3;
    vec[4]=2;
    vec[5]=9;
    vec[6]=0; // 7 o 0 para ver que hay menos ciclos
    vec[7]=1;
    vec[8]=2;
    vec[9]=4;

    printf("Vector inicializado: ");
    mostrarVector(vec);
    printf("\n");
}

void inicializarVector4(int* vec){
    vec[0]=1;
    vec[1]=5;
    vec[2]=2;
    vec[3]=3;

    printf("Vector inicializado: ");
    mostrarVector(vec);
    printf("\n");
}

void inicializarVector(int* vec){
    inicializarVector4(vec);
    //inicializarVector10(vec);
}

void intercambio (int* a, int* b){
    int c;

    c=*a;
    *a=*b;
    *b=c;
}

void mostrarVector(int vec[]){
    int i;

    printf("Vector: ");
    for (i=0 ; i<DIM ; i++){
        printf("%d ",vec[i]);
    }
    printf("\n");
}

void f1_ordenamientoBurbuja(int vec[], int dim){
    int i, j, aux, cambio;
    /*
    Por la condicion del if, el algoritmo va guardando el valor mas alto
    en la ultima posicion
    */
    for (i = 0; i < dim-1; i++) {
        printf("i=%d\n",i);
        for (j = 0; j < dim-i-1; j++){
            printf(" j=%d: %d %d ",j,vec[j],vec[j+1]);
            if (vec[j] > vec[j+1]){
                intercambio(&vec[j], & vec[j+1]);
                printf("Intercambio!");
            }
            printf("\n");
        }
        printf(" vec[%d]=%d\n",j,vec[j]);
    }
}

void f2_ordenamientoBurbujaMejorado(int v[], int d){
    int i,j,aux, ordenado;
    /* Ordenamiento*/
    for (i = 0; i < d-1; i++) {
        ordenado=1;
        printf("i=%d (ordenado=%d)\n",i,ordenado);
        for (j = 0; j < d-i-1; j++){
            printf(" j=%d: %d %d ",j,v[j],v[j+1]);
            if (v[j] > v[j+1]){
                intercambio(&v[j], &v[j+1]);
                ordenado=0;
                printf("Intercambio! (ordenado=0)");
            }
            printf("\n");
        }
        printf(" vec[%d]=%d\n",j,v[j]);
        if (ordenado==1){
            i=d; /* si en el for interno no hay intercambio, esta ordenado */
            printf("Completamente ordenado!\n");
            return;
        }
    }
 }

void f3_ordenarPorInsercion(int v[], int d){
    int k, cont, aux;
    /* Ordenar y mostrar resultados intermedios*/
    mostrarVector(v);
    for (cont = 1 ; cont < d ; cont++){
        /* Colocar v[cont] */
        mostrarVector(v);
        aux = v[cont];
        k = cont-1; /* posicion del elemento a comparar */
        printf("cont:%d aux=%d k=%d\n",cont,aux,k);
        while ((v[k] > aux) && (k>0)){
            /* Desplazar elementos */
            printf(" Desplazando: v[%d]:%d\n",k,v[k]);
            v[k+1] = v[k];
            k--;
        }
        if (v[k] <= aux){
            /* posicion intermedia */
            printf(" Posicion intermedia\n");
            v[k+1] = aux;
        }
        else { /* colocar el primero */
            printf(" Colocar en primero\n");
            v[1] = v[0];
            v[0] = aux;
        }
    }
}

void f4_quicksort(int a[], int primero, int ultimo){
    int contint=0, pivote, i, j, central, tmp,c;
    central = (primero + ultimo)/2;
    pivote = a[central];
    i = primero;
    j = ultimo;
    printf("Primero:%d Ultimo:%d Pivote:%d\n",primero,ultimo,pivote);
    mostrarVector(a);
    do {
        while (a[i] < pivote){
            printf("  inc i\t");
            i++;
        }
        printf("\n");
        while (a[j] > pivote){
            printf("  inc j\t");
            j--;
        }
        printf("\n");
        if (i<=j){
            printf("  intercambio a[%d]:%d a[%d]:%d\n",i,a[i],j,a[j]);
            tmp = a[i];
            a[i] = a[j];
            a[j] = tmp;
            i++;
            j--;
        }
    } while (i <= j);
    if (primero < j)
        f4_quicksort(a, primero, j);
    if (i < ultimo)
        f4_quicksort(a, i, ultimo);
}

int f5_busquedaSecuencial(int v[], int dim, int val){
    /*
    No requiere que el vector este ordenado
    Devuelve la posicion de la primer ocurrencia
    */
    int i;
    for (i=0 ; i<dim ; i++) {
        if (v[i]==val)
            return i;
        }
    return -1;
}

int f6_busquedaBinaria(const int b[], int claveDeBusqueda, int bajo, int alto, int d){
    int central;
    printf("bajo=%d alto=%d\n",bajo,alto);
    while ( bajo <= alto ) {
        central = ( bajo + alto ) / 2;
        printf("  central=%d (valor=%d)\n",central,b[central]);
        if ( claveDeBusqueda == b[central] ) {
            printf("  claveDeBusqueda es el central!\n");
            return central;
        }
        else if ( claveDeBusqueda < b[central] ) {
            alto = central - 1;
            printf("  alto=%d\n",alto);
        }
        else {
            bajo = central + 1;
            printf("  bajo=%d\n",bajo);
        }
    }
    return -1;
}

int menu(){
    int opt;
    printf("*** Ordenamientos y busquedas ***\n");
    printf("1- Ordenamiento burbuja\n");
    printf("2- Ordenamiento burbuja mejorado\n");
    printf("3- Ordenamiento por insercion\n");
    printf("4- Orenamiento Quick Sort\n");
    printf("5- Busqueda secuencial\n");
    printf("6- Busqueda binaria\n");
    printf("0- Salir\n");
    scanf("%d",&opt);
    return opt;
}

int main(){
    int opcion, valor,buscado;
    int numeros[DIM];

    opcion=menu();

    while (opcion != 0){
        inicializarVector(numeros);
        switch(opcion){
        case 1:
            f1_ordenamientoBurbuja(numeros,DIM);
            mostrarVector(numeros);
            printf("\n\n");
            break;
        case 2:
            //f1_ordenamientoBurbuja(numeros,DIM);
            //printf("\n\n***VECTOR YA ORDENADO***\n\n");
            f2_ordenamientoBurbujaMejorado(numeros,DIM);
            mostrarVector(numeros);
            printf("\n\n");
            break;
        case 3:
            //f1_ordenamientoBurbuja(numeros,DIM);
            //printf("\n\n***VECTOR YA ORDENADO***\n\n");
            f3_ordenarPorInsercion(numeros,DIM);
            mostrarVector(numeros);
            printf("\n\n");
            break;
        case 4:
            //f1_ordenamientoBurbuja(numeros,DIM);
            //printf("\n\n***VECTOR YA ORDENADO***\n\n");
            f4_quicksort(numeros,0,DIM);
            mostrarVector(numeros);
            printf("\n\n");
            break;
        case 5:
            f1_ordenamientoBurbuja(numeros,DIM);
            printf("\n\n***VECTOR YA ORDENADO***\n\n");
            valor=f5_busquedaSecuencial(numeros,DIM,2);
            printf("El valor %d esta en la posicion %d\n",2,valor);
            valor=f5_busquedaSecuencial(numeros,DIM,7);
            printf("El valor %d esta en la posicion %d\n",7,valor);
            printf("\n\n");
            break;
        case 6:
            f1_ordenamientoBurbuja(numeros,DIM);
            printf("\n\n***VECTOR YA ORDENADO***\n\n");
            buscado=6;
            mostrarVector(numeros);
            valor=f6_busquedaBinaria(numeros,buscado,0,DIM-1,DIM);
            printf("El valor %d esta en la posicion %d\n\n",buscado,valor);
            buscado=9;
            mostrarVector(numeros);
            valor=f6_busquedaBinaria(numeros,buscado,0,DIM-1,DIM);
            printf("El valor %d esta en la posicion %d\n\n",buscado,valor);
            printf("\n");
            break;
        default:
            printf("*** OPCION INVALIDA ***\n\n\n");
        }
        opcion=menu();
    }
    return 0;
    }
