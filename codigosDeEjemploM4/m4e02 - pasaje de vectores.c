/*
En este codigo fuente se muestra c�mo se trabaja con vectores
*/
#include <stdio.h>
#define DIM 10

void f1_pasajeDeVector(int* vec, int tam){
    int i=0;

    printf("Direccion inicial de vec: %x\n",vec);
    for (i=0 ; i<tam ; i++)
        printf("%d|",vec[i]);
    printf("\n");
    printf("Direccion final de vec: %x\n",vec);
    printf("Valor al que apunta vec: %d\n",*vec);
}

void f2_pasajeDeVector(int* vec, int tam){
    int i=0;

    printf("Direccion inicial de vec: %x\n",vec);
    for (i=0 ; i<tam ; i++,vec++)
        printf("%d|",*vec);
    printf("\n");
    printf("Direccion final de vec: %x\n",vec);
    printf("Valor al que apunta vec: %d\n",*vec);
}

void f3_pasajeDeVector(int vec[], int tam){
    int i=0;

    printf("Direccion inicial de vec: %x\n",vec);
    for (i=0 ; i<tam ; i++)
        printf("%d|",vec[i]);
    printf("\n");
    printf("Direccion final de vec: %x\n",vec);
    printf("Valor al que apunta vec: %d\n",*vec);
}

int menu(){
    int opcion;

    printf("*** Vectores ***\n");
    printf("1- f1\n");
    printf("2- f2\n");
    printf("3- f3\n");
    printf("0- Salir\n");
    scanf("%d",&opcion);
    return opcion;
}

int main(){
    int opcion;
    int vec[DIM]={0,1,2,3,4,5,6,7,8,9};

    opcion=menu();
    while (opcion!=0){
        switch(opcion){
            case 1:
                f1_pasajeDeVector(vec,DIM);
                break;
            case 2:
                f2_pasajeDeVector(vec,DIM);
                break;
            case 3:
                f3_pasajeDeVector(vec,DIM);
                break;
            default:
                printf("Opcion invalida!\n");
                printf("\n\n");
                break;
        }
        opcion=menu();
    }

    return 0;
    }
