/*
En este codigo fuente se muestra c�mo se trabaja con estructuras
*/
#include <stdio.h>
#define DIM 4
#define TAMCADENA 15

// estructura que modela una materia
struct materia{
    int idMateria;
    char nombre[TAMCADENA];
    int notas[DIM]; // son notas de un alumno
};

// estructura que modela un alumno
struct alumno {
    int legajo;
    char nombre[TAMCADENA];
    char apellido[TAMCADENA];
    struct materia materias[DIM]; // vector de materias
};

// definiendo una materia como un tipo de dato
typedef struct {
    int idMateria;
    char nombre[TAMCADENA];
    int notas[DIM];
} Materia;

// definicion de una enumeracion
enum dias {DOM, LUN, MAR, MIER, JUE, VIE, SAB};
// DOM=0, LUN=1, etc...

typedef enum {
    BLANCO=5, AMARILLO, ROJO, VERDE, AZUL, MARRON, NEGRO
    } color;


void f1_estructurasAnidadas(){
    int i;
    // inicializamos las estructuras
    struct materia mat1 = {1001,"Matematica",{4,6,8,9}};
    struct materia mat2 = {1004,"Algrebra",{4,6,8,9}};
    struct materia mat3 = {1015,"Economia",{4,6,8,9}};
    struct materia mat4 = {2045,"Programacion_1",{4,6,8,9}};
    // utilizamnos las variables anteriores para inicializar otra estructura
    struct alumno unAlumno = {98767,"Aquiles","Bailoyo",{mat1,mat2,mat3,mat4}};

    // mostramos las materias cargadas en el alumno
    printf("El alumno %s %s cursa:\n",unAlumno.nombre, unAlumno.apellido);
    for (i=0 ; i<DIM ; i++){
        printf("%d) %s\n",i+1,unAlumno.materias[i].nombre);
    }
}

void f2_declaracionDeTipoDeDato(){
    // declaracion usando la estrucutra
    struct materia mat1 = {1001,"Matematica",{4,6,8,9}};
    // declaracion usando el tipo de dato basado en una estructura
    Materia mat2 = {1001,"Economia",{4,6,8,9}};

    printf("Las materias son %s y %s\n",mat1.nombre, mat2.nombre);
}

void f3_enumeraciones(){
    // declaracion de una variable del tipo de dato de la enumeracion
    enum dias diasDeSemana;
    color unColor=ROJO;
    /*
    Las enumeraciones sirven para asociar una palabra/constante a un valor
    */
    diasDeSemana=MIER;
    if (diasDeSemana==MIER){
        printf("El dia es Miercoles y el valor de diasDeSemana es %d\n", MIER);
    }
    switch (unColor){
    case BLANCO:
        printf("El color asignado es blanco (%d)!\n",unColor);
        break;
    case ROJO:
        printf("El color asignado es rojo (%d)!\n",unColor);
        break;
    case VERDE:
        printf("El color asignado es verde (%d)!\n",unColor);
        break;
    default:
        printf("El color no importa\n");
    }
}

int menu(){
    int opt;
    printf("*** Estructuras 2 ***\n");
    printf("1- Estructuras anidadas\n");
    printf("2- Declaracion de un tipo de dato\n");
    printf("3- enumeraciones\n");
    printf("0- Salir\n");
    scanf("%d",&opt);
    return opt;
}

int main(){
    int opcion;

    opcion=menu();

    while (opcion != 0){
        switch(opcion){
        case 1:
            f1_estructurasAnidadas();
            printf("\n\n");
            break;
        case 2:
            f2_declaracionDeTipoDeDato();
            printf("\n\n");
            break;
        case 3:
            f3_enumeraciones();
            printf("\n\n");
            break;
        default:
            printf("*** OPCION INVALIDA ***\n\n\n");
        }
        opcion=menu();
    }
    return 0;
    }
