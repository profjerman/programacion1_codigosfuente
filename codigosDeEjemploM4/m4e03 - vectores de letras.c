/*
En este codigo fuente se muestra c�mo se trabaja con vectores de letras
*/

#include <stdio.h>
#define DIM 15

int f1_declaracion(){
    int i;
    /* asi se declara un vector de letras*/
    char vec2[] = {'a','b','c','d'}; /* no especifico tama�o pero toma el tama�o de la cantidad de elementos */
    /*
    carga del vector por posicion,
    est� o no iniciado
    */
    printf("Declaracion de vectores, asignacion y acceso de valores\n");
    vec2[0]=vec2[0] + 5; /* Cual es la letra que esta 5 lugares despues? */
    vec2[2]=55; /* que valor es el 55 en la tabla ASCII? */
    vec2[3]='z'; /* asi se indica que lo que se guarda es el valor ASCII de la 'z' */
    /* mostrar datos del vector */
    printf("Algunos datos: %c %c %c\n", vec2[0], vec2[2],vec2[3]);
    printf("\n\n");
}

int f2_recorriendo(){
    /* ciclo que recorre el vector */
    char i, vec1[DIM]={'M','a','r','t','i','n'};
    // vec1 se inicia con Martin y el resto en chars vacios

    printf("Mostrar vector de letras\n");
    for (i=0 ; i<DIM ; i++)
        printf("%d-%c\n",i,vec1[i]);
    printf("Me sobran espacios para las letras!!\n\n");
}

void actualizarVector(char* unVector, int dim){
    int i;
    float sum;

    // A cada valor el sumamos 32 (que en ASCII es...)
    for (i=0 ; i<DIM ; i++)
        unVector[i] += 32;
    printf("\n\n");
}

void mostrarVector(char* unVector, int dim){
    int i;

    printf("Mostrar vector que vino por parametro:\n|"); /* ver detalle del pipe */
    for (i=0 ; i<dim ; i++){
        printf("%c|",unVector[i]);
        }
    printf("\n\n");
}

int menu(){
    int opcion;

    printf("*** Vectores de letras ***\n");
    printf("1- Declaracion\n");
    printf("2- Recorrido\n");
    printf("3- Mostrar el vector\n");
    printf("4- Actualizar el vector\n");
    printf("0- Salir\n");
    printf("\n");

    scanf("%d",&opcion);
    return opcion;
}

int main(){
    char vec[DIM]={'I','N','S','P','T','-','U','T','N'};
    int opcion;

    opcion=menu();

    while (opcion!=0){
        switch(opcion){
            case 1:
                f1_declaracion();
                break;
            case 2:
                f2_recorriendo();
                break;
            case 3:
                mostrarVector(vec,DIM);
                break;
            case 4:
                actualizarVector(vec,DIM);
                break;
            default:
                printf("Opcion invalida\n");
                break;
        }
        opcion=menu();
    }
    return 0;
    }
