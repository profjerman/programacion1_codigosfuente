/*
En este codigo fuente se muestra c�mo se trabaja con vectores multidimensionales, o matrices
*/
#include <stdio.h>
#include <string.h>
#define DIM 3

void f1_cargarMatriz1(int mat1[][DIM],int tam1,int tam2){
    int i,j;

    for (i=0 ; i<tam1 ; i++){
        for (j=0 ; j<tam2 ; j++){
            mat1[i][j]=i+j;
        }
    }
    printf("Matriz cargada!\n");
}

void f2_mostrarMatriz1(int mat1[][DIM],int tam1,int tam2){
    int i,j;

    printf("La matriz cargada es:\n");
    for (i=0 ; i<tam1 ; i++){
        for (j=0 ; j<tam2 ; j++){
            printf("%d-%d:%d\t",i,j,mat1[i][j]);
        }
        printf("\n");
    }
}

void f3_cargarMatriz2(int matriz[][DIM][DIM], int tam1, int tam2, int tam3){
    int i,j,k;

    for (i=0 ; i<tam1 ; i++)
        for (j=0 ; j<tam2 ; j++)
            for (k=0 ; k<tam3 ; k++)
                if (i==j && j==k)
                    matriz[i][j][k]=1;
                else
                    matriz[i][j][k]=0;
    printf("Matriz cargada!\n");
}

void f4_mostrarMatriz2(int matriz[][DIM][DIM], int tam1, int tam2, int tam3){
    int i,j,k;

    for (i=0 ; i<tam1 ; i++){
        for (j=0 ; j<tam2 ; j++){
            for (k=0 ; k<tam3 ; k++){
                printf("%d-%d-%d:%d  ",i,j,k,matriz[i][j][k]);
            }
            printf("\n"); // salto de linea de una dimension
        }
        printf("\n"); // salto de linea de la segunda dimension
    }
}

void f5_mostrarMatriz2bis(int matriz[][DIM][DIM], int tam1, int tam2, int tam3){
    int i,j,k;

    for (i=0 ; i<tam1 ; i++){
        for (j=0 ; j<tam2 ; j++){
            printf("->%i:\n",i);
            printf("-->%d:\n",j);
            for (k=0 ; k<tam3 ; k++){
                printf("--->%d:%d\n",k,matriz[i][j][k]);
            }
            printf("\n");
        }
    }
}

int menu(){
    int opt;

    printf("*** Matrices ***\n");
    printf("1- Cargar matriz bi-dimensional\n");
    printf("2- Mostrar matriz bi-dimensional\n");
    printf("3- Cargar matriz tri-dimensional\n");
    printf("4- Mostrar matriz tri-dimensional (tabla)\n");
    printf("5- Mostrar matriz tri-dimensional (arbol)\n");
    printf("0- Salir\n");
    scanf("%d",&opt);
    return opt;
}

int main(){
    int mat1[DIM][DIM], mat2[DIM][DIM][DIM];
    int opcion;

    opcion=menu();

    while (opcion != 0){
        switch(opcion){
        case 1:
            f1_cargarMatriz1(mat1,DIM,DIM);
            printf("\n\n");
            break;
        case 2:
            f2_mostrarMatriz1(mat1,DIM,DIM);
            printf("\n\n");
            break;
        case 3:
            f3_cargarMatriz2(mat2,DIM,DIM,DIM);
            printf("\n\n");
            break;
        case 4:
            f4_mostrarMatriz2(mat2,DIM,DIM,DIM);
            printf("\n\n");
            break;
        case 5:
            f5_mostrarMatriz2bis(mat2,DIM,DIM,DIM);
            printf("\n\n");
            break;
        default:
            printf("*** OPCION INVALIDA ***\n");
        }
        opcion=menu();
    }
    return 0;
    }
