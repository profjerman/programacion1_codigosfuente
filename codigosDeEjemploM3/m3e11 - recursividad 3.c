#include <stdio.h>
#include <stdlib.h>

int factorial(int unNumero){

    // este IF hace que la funcion se "a prueba de todo",
    // si el valor no es valido, el resultado que se devuelve es de error
    if (unNumero<0)
        return -1;

    if(unNumero==0)
        return 1;
    else
        return unNumero * factorial(unNumero-1);
    // abajo se ve como se apilan las llamadas recursivas entre parentesiss
    // 5 * (4 * (3 * (2 * (1 * (1)))))
}

int main()
{
    int x=0;

    // evaluando caso base
    printf("Factorial de %d es %d\n",x,factorial(x));
    x=1;
    printf("Factorial de %d es %d\n",x,factorial(x));
    x=5;
    printf("Factorial de %d es %d\n",x,factorial(x));
    x=3;
    printf("Factorial de %d es %d\n",x,factorial(x));
    x=-9;
    printf("Factorial de %d es %d\n",x,factorial(x));
    return 0;
}

