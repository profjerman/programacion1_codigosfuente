#include <stdio.h>
#include <stdlib.h>
/*
El pasaje por referencia
*/


int main() {
    int unNumero,i=1;
    int cantPares=0,cantImpares=0,cantCeros=0;

    do {
        printf("Ingrese numero #%d:",i);
        scanf("%d",&unNumero);
        contabilizarNumeros(unNumero,&cantPares,&cantImpares,&cantCeros);
        i++;
    } while (unNumero!=99);

    printf("Pares:%d Impares:%d Ceros:%d\n",cantPares,cantImpares,cantCeros);
    printf("Cantidad total:%d\n",cantCeros+cantImpares+cantPares);
    return 0;
}

void contabilizarNumeros(int num, int* cantP, int* cantI, int* cantC){
    int resto;

    if (num == 0){
        (*cantC)++;
    }else{
        if (num%2==0){
            (*cantP)++;
        } else {
            (*cantI)++;
        }
    }
}
