#include <stdio.h>
#include <stdlib.h>

int sumatoriaNoRecursiva (int x){
    int i,acum=0;

    for (i=0 ; i<=x ; i++){
        acum+=i;
    }
    return acum;
}

int sumatoriaRecursiva (int x){

    if (x==0){
        return 0;
    } else {
        return x + sumatoriaRecursiva(x-1);
    }
}

int main()
{
    int x=3;
    printf("Sumatoria No recursiva de %d es %d\n",x,sumatoriaNoRecursiva(x));
    printf("Sumatoria recursiva de %d es %d\n",x,sumatoriaRecursiva(x));

    return 0;
}

