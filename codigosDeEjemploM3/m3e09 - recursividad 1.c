#include <stdio.h>
#include <stdlib.h>

void saludarNveces (int n) {
    int i;

    for (i=0 ; i<n ; i++)
        printf("%d) Hola Flanders!\n",i);
}

void saludarNvecesRecuriva (int n) {
    int i;

    if (n==0){
        ;
    } else {
        printf("%d) Hola Flanders!\n",n);
        saludarNvecesRecuriva(n-1);
    }
}

int main()
{
    int x=3;
    saludarNveces(x);
    printf("\n");
    saludarNvecesRecuriva(x);
    return 0;
}

