#include <stdio.h>
#include <stdlib.h>
/*
El pasaje por referencia
*/
// funcion con pasaje por valor
void duplicarA(int valor);
// funcion con pasaje por referencia
void duplicarB(int* valor);

int main() {
    int unNumero=4;

    printf("Muestra 1 - numero vale: %d\n",unNumero);
    duplicarA(unNumero);
    printf("Muestra 2 - numero vale: %d\n",unNumero);
    duplicarB(&unNumero);
    printf("Muestra 3 - numero vale: %d\n",unNumero);
    return 0;
}

void duplicarA(int valor){
    valor*=2;
    printf("DuplicarA - valor vale: %d\n",valor);
}

void duplicarB(int* valor){
    // estos parentesis no son necesarios
    (*valor)*=2;
    printf("DuplicarB - valor vale: %d\n",*valor);
}

