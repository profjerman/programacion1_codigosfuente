#include <stdio.h>
#include <stdlib.h>
/*
El pasaje por referencia
*/
void promediarNotas(float* prom);

int main() {
    int legajo;
    float promedio;

    printf("Ingrese un legajo: ");
    scanf("%d",&legajo);
    promediarNotas(&promedio);
    printf("El promedio del legajo %d es: %2.2f\n", legajo, promedio);

    return 0;
}

void promediarNotas(float* prom){
    int nota=0,i=0,acum=0;

    for (i=0 ; i<3 ; i++){
        printf("Ingrese nota: ");
        scanf("%d",&nota);
        acum+=nota;
    }
    (*prom)=acum/(float)i;
    //printf("%f %d %d\n",*prom, acum,i);
}
