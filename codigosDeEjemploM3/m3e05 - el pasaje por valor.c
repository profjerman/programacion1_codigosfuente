#include <stdio.h>
#include <stdlib.h>

void duplicar(int);

int main(){
    int valor=50;

    printf("El valor es %d\n", valor );

    duplicar( valor );

    printf("El valor ahora es %d\n", valor );

    return 0;
}

void duplicar( int unValor ){

    unValor = unValor * 2;

    printf("El duplicado vale %d\n", unValor );
}
