/*
En este codigo fuente se muestra c�mo se trabaja con memoria dinamica
*/
#include <stdio.h>
#include <string.h>
#define DIM 10

// esta es la estructura ne nuestro nodo o "eslabon de lista"
typedef struct nodo {
    int legajo;         // campo con dato
    int edad;           // campo con dato
    char nombre[DIM];   // campo con dato
    struct nodo *sgte;  // campo puntero al nodo siguiente. Notar que apunta a un tipo de dato igual al propio nodo
} nodo;
// como veran, la definicion de esta estructura es "recursiva" (un campo es un punteo a una estructura del mismo tipo de dato que la propia estructura)

void banner(){
    printf("*******************************************\n");
    printf("*** Aprendiendo a usar memoria dinamica ***\n");
    printf("*******************************************\n");
    printf("\n");
}

int menu(){
    int a;
    printf("****** Menu ******\n");
    printf("Elija una opcion:\n");
    printf("0- Salir del programa\n");
    printf("1- Crear nodos\n");
    printf("2- Enganchando nodos\n");
    printf("3- Mostrar estructura de nodos\n");
    printf("4- Pilas\n");
    printf("5- Colas\n");
    printf("6- Listas\n");
    scanf("%d",&a);
    return a;
}

void errores(int i){
    /*
    Funcion de manejo de errores centralizado
    */
    switch(i){
        case 1:
            printf("Error %d: Opcion invalida!\n",i);
            break;
    }
}

nodo* crearNodo(int legajo, int edad, char* nombre){
    nodo* nodoNuevo;

    nodoNuevo=(nodo*)malloc(sizeof(nodo)); // asignamos un espacio del tama�o del registro a nuestro puntero
    nodoNuevo->legajo=legajo; // cargamos nuestro puntero con varios campos
    nodoNuevo->edad=edad;
    strcpy(nodoNuevo->nombre,nombre);
    nodoNuevo->sgte=NULL; // el puntero al nodo siguiente siempre se inicializa con NULL
    return nodoNuevo;
}

void f1_crearNodos(){
    nodo *nodo1=NULL, *nodo2=NULL;

    nodo1=crearNodo(123,23,"Matias");
    nodo2=crearNodo(456,24,"Florencia");
    printf("nodo1: %d-%d-%s\n",nodo1->legajo,nodo1->edad,nodo1->nombre);
    printf("nodo2: %d-%d-%s\n",nodo2->legajo,nodo2->edad,nodo2->nombre);
    printf("\n");
}

nodo* crearEstructuraDenodos(){
    nodo *nodo1=NULL, *nodo2=NULL, *nodo3=NULL, *nodo4=NULL;

    // creo los nodos
    nodo1=crearNodo(123,23,"Matias");
    nodo2=crearNodo(456,24,"Florencia");
    nodo3=crearNodo(789,25,"Mauricio");
    nodo4=crearNodo(101,26,"Magali");
    // enlazo los nodos para armar la estructura
    nodo1->sgte=nodo2;
    nodo2->sgte=nodo3;
    nodo3->sgte=nodo4;
    // devuelvo el nodo "cabeza" o "cabecera" para mantener los demas nodos referenciados
    return nodo1;
}

void f2_enganchandoModos(){
    nodo *cadenaDeNodos;

    cadenaDeNodos=crearEstructuraDenodos();
    printf("Ahora desde el nodo 'cabeza' puedo llegar a los demas!!\n");
    printf("Nombre del nodo 1 referenciado desde la cabeza: %s\n",cadenaDeNodos->nombre);
    printf("Nombre del nodo 2 referenciado desde la cabeza: %s\n",cadenaDeNodos->sgte->nombre);
    printf("Nombre del nodo 3 referenciado desde la cabeza: %s\n",cadenaDeNodos->sgte->sgte->nombre);
    printf("\n");
}

void mostrarEstructuraDeNodos(nodo* nodo){

    if (nodo==NULL){
        return;
    } else {
        printf("leg=%d|edad=%d|nom=%s\n",nodo->legajo,nodo->edad,nodo->nombre);
        mostrarEstructuraDeNodos(nodo->sgte);
    }
    /*
    while (nodo != NULL){
        printf("leg=%d|edad=%d|nom=%s\n",nodo->legajo,nodo->edad,nodo->nombre);
        nodo=nodo->sgte;
    }
    */
}

void f3_mostrarEstructuraDeNodos(){
    nodo* cadenaDeNodos;

    cadenaDeNodos=crearEstructuraDenodos();
    printf("Mostrando lista...\n");
    mostrarEstructuraDeNodos(cadenaDeNodos);
    printf("\n");
}

void apilar(nodo** pila, nodo* nodoNuevo){
    // IMPORTANTE: como vamos a modificar el valor de la direccion de memoria a la que apunta pila,
    // SE DEBE pasar el puntero por referencia, es decir, doble asterisco!!
    nodoNuevo->sgte=*pila;
    *pila=nodoNuevo;
}

nodo* desapilar(nodo** pila){
    // igual que en el insertar, al modificar la cabeza de la pila, el puntero DEBE pasarse por referencia
    nodo* nodoAuxiliar;

    nodoAuxiliar=*pila;
    (*pila)=(*pila)->sgte;
    nodoAuxiliar->sgte=NULL;
    return nodoAuxiliar;
}

void f4_Pilas(){
    // Las pilas son estructuras FILO: First In Last Out (el primero que entra es el ultimo que sale)
    nodo* miPila=NULL, *nodoAuxiliar;
    printf("Pilas: estruturas FILO\n");

    nodoAuxiliar=crearNodo(123,23,"Matias");
    apilar(&miPila,nodoAuxiliar);
    nodoAuxiliar=crearNodo(456,24,"Florencia");
    apilar(&miPila,nodoAuxiliar);
    nodoAuxiliar=crearNodo(789,25,"Mauricio");
    apilar(&miPila,nodoAuxiliar);
    nodoAuxiliar=crearNodo(101,26,"Magali");
    apilar(&miPila,nodoAuxiliar);
    printf("Mostrando la pila desde el nodo cabeza\n");
    mostrarEstructuraDeNodos(miPila);
    printf("\n");

    printf("Desapilando un nodo de la pila\n");
    nodoAuxiliar=desapilar(&miPila);
    printf("Mostrando la pila desde el nodo cabeza (leg=%d)\n",nodoAuxiliar->legajo);
    mostrarEstructuraDeNodos(miPila);
    printf("\n");
}

void encolar(nodo** cola, nodo* nodoNuevo){

    if ( (*cola)==NULL ){
        *cola=nodoNuevo;
    }else{
        encolar( &((*cola)->sgte), nodoNuevo );
    }
}

nodo* desencolar(nodo** cola){
    nodo* nodoAuxiliar;

    nodoAuxiliar=*cola;
    *cola=(*cola)->sgte;
    nodoAuxiliar->sgte=NULL;
    return nodoAuxiliar;
}

void f5_colas(){
    // Las colas son estructuras FIFO: First In First Out (el primero que entra es el primero que sale)
    nodo* miCola=NULL, *nodoAuxiliar;
    printf("Colas: estruturas FIFO\n");

    nodoAuxiliar=crearNodo(123,23,"Matias");
    encolar(&miCola,nodoAuxiliar);
    nodoAuxiliar=crearNodo(456,24,"Florencia");
    encolar(&miCola,nodoAuxiliar);
    nodoAuxiliar=crearNodo(789,25,"Mauricio");
    encolar(&miCola,nodoAuxiliar);
    nodoAuxiliar=crearNodo(101,26,"Magali");
    encolar(&miCola,nodoAuxiliar);
    printf("Mostrando la Cola desde el nodo cabeza\n");
    mostrarEstructuraDeNodos(miCola);
    printf("\n");

    printf("Desapilando un nodo de la pila\n");
    nodoAuxiliar=desencolar(&miCola);
    printf("Mostrando la pila desde el nodo cabeza (leg=%d)\n",nodoAuxiliar->legajo);
    mostrarEstructuraDeNodos(miCola);
    printf("\n");
}

void insertarOrdenado(nodo** lista, nodo* nodoNuevo){
    // https://www.youtube.com/watch?v=ycbqnwFb43Y&ab_channel=Programaci%C3%B3nDesdeCero
    nodo *nodoAuxiliar;

    printf("Insertando legajo: %d\n",nodoNuevo->legajo);
    if ( (*lista) == NULL || nodoNuevo->legajo < (*lista)->legajo ){
        nodoNuevo->sgte=(*lista);
        (*lista)=nodoNuevo;
    } else {
        nodoAuxiliar=(*lista);
        nodoAuxiliar=(*lista);
        while(nodoAuxiliar->sgte!=NULL && nodoAuxiliar->sgte->legajo < nodoNuevo->legajo){
            nodoAuxiliar=nodoAuxiliar->sgte;
        }
        if (nodoAuxiliar->sgte!=NULL){
            nodoNuevo->sgte=nodoAuxiliar->sgte;
        }
        nodoAuxiliar->sgte=nodoNuevo;
    }
}

void eliminarNodo(nodo** lista,int unLegajo){
    nodo *nodoAuxiliar=*lista;
    nodo *nodoAnterior=nodoAuxiliar;
    int i=0;

    printf("Eliminando nodo de legajo %d...\n",unLegajo);
    while (nodoAuxiliar!=NULL){
        if (nodoAuxiliar->legajo==unLegajo){
            printf("Legajo eliminado!\n");
            nodoAnterior->sgte=nodoAuxiliar->sgte;
            nodoAuxiliar=NULL;
        }else{
            nodoAnterior=nodoAuxiliar;
            nodoAuxiliar=nodoAuxiliar->sgte;
        }
    }
}

nodo* buscarNodo(nodo** lista,int legajo){
    nodo *nodoAuxiliar=*lista;

    while (nodoAuxiliar!=NULL){
        if (nodoAuxiliar->legajo==legajo){
            return nodoAuxiliar;
        }else{
            nodoAuxiliar=nodoAuxiliar->sgte;
        }
    }
}

void liberarLista(nodo** lista){
    nodo *nodoAuxiliar=*lista;
    int i=1;

    while (*lista!=NULL){
        free(nodoAuxiliar);
        *lista=(*lista)->sgte;
    }
}

void f6_listas(){
    // Las listas son estructuras que se pueden utilizar agregando o quitando arbitrariamente
    nodo* miLista=NULL, *nodoAuxiliar;
    int legajoBuscado;
    printf("Listas: estruturas de nodos\n");

    nodoAuxiliar=crearNodo(123,23,"Matias");
    insertarOrdenado(&miLista,nodoAuxiliar);
    nodoAuxiliar=crearNodo(456,24,"Florencia");
    insertarOrdenado(&miLista,nodoAuxiliar);
    nodoAuxiliar=crearNodo(789,25,"Mauricio");
    insertarOrdenado(&miLista,nodoAuxiliar);
    nodoAuxiliar=crearNodo(101,26,"Magali");
    insertarOrdenado(&miLista,nodoAuxiliar);
    printf("Mostrando la Lista desde el nodo cabeza\n");
    mostrarEstructuraDeNodos(miLista);

    nodoAuxiliar=NULL;
    printf("Buscando nodos...\n");
    legajoBuscado=456;
    nodoAuxiliar=buscarNodo(&miLista,legajoBuscado);
    if (nodoAuxiliar!=NULL){
        eliminarNodo(&miLista,legajoBuscado);
    }
    legajoBuscado=321;
    nodoAuxiliar=buscarNodo(&miLista,legajoBuscado);
    if (nodoAuxiliar!=NULL){
        eliminarNodo(&miLista,legajoBuscado);
    }

    printf("Mostrando la Lista desde el nodo cabeza\n");
    mostrarEstructuraDeNodos(miLista);
    printf("Liberando lista...\n");
    liberarLista(&miLista);
    printf("\n");
}

int main(){
    int opcion;
    FILE* unArchivo;

    banner();
    opcion=menu();
    while(opcion!=0){
        switch(opcion){
            case 0:
                return 0;
            case 1:
                f1_crearNodos();
                break;
            case 2:
                f2_enganchandoModos();
                break;
            case 3:
                f3_mostrarEstructuraDeNodos();
                break;
            case 4:
                f4_Pilas();
                break;
            case 5:
                f5_colas();
                break;
            case 6:
                f6_listas();
                break;
            default:
                errores(1);
                break;
        }
        opcion=menu();
    }
    return 0;
    }
