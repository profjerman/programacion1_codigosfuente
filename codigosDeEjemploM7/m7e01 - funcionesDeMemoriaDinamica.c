/*
En este codigo fuente se muestra c�mo se trabaja con memoria dinamica
*/
#include <stdio.h>
#define DIM 10

void banner(){
    printf("*******************************************\n");
    printf("*** Aprendiendo a usar memoria dinamica ***\n");
    printf("*******************************************\n");
    printf("\n");
}

int menu(){
    int a;
    printf("****** Menu ******\n");
    printf("Elija una opcion:\n");
    printf("0- Salir del programa\n");
    printf("1- Malloc() (memory allocation)\n");
    printf("2- Malloc() 2\n");
    printf("3- Calloc()\n");
    printf("4- Realloc()\n");
    printf("5- Free()\n");
    printf("6- Matriz dinamica\n");
    scanf("%d",&a);
    return a;
}

void errores(int i){
    /*
    Funcion de manejo de errores centralizado
    */
    switch(i){
        case 1:
            printf("Error %d: Opcion invalida!\n",i);
            break;
    }
}

void f1_malloc(){
    // reserva un espacio de memoria de un tama�o definido en bytes que se pasa por par�metro
    // void* malloc(size_t tama�o);
    int *ptr, i;

    printf("\n");
    ptr=(int*)malloc(sizeof(int));
    *ptr=7;
    i=*ptr;
    printf("i=%d\n",i);
    printf("\n");
}

void f2_malloc(){
    int *vec,i;

    printf("\nUsamos malloc para crear un espacio de %d bytes\n",DIM * sizeof(int));
    vec=(int*)malloc(DIM * sizeof(int)); // este espacio de memoria es CONTIGUO
    for (i=0;i<DIM;i++){
        vec[i]=i+2;
    }
    printf("Los valores cargados en el espacio de memoria son:\n");
    for (i=0;i<DIM;i++){

        printf("vec[%d]=%d   ",i,vec[i]);
    }
    printf("\n\n");
}

void f3_calloc(){
    //  es similar a malloc, asigna el n�mero especificado de bytes y los inicializa a cero
    // void* calloc (size_t cantElem, size_t size);
    int *vec,i;

    printf("\nCreando vector de %d posiciones\n",DIM);
    vec=(int*)calloc(DIM,sizeof(int)); // este espacio de memoria es CONTIGUO
    printf("Mostrando el contenido del vector\n");
    for (i=0;i<DIM;i++){
        printf("%d ",vec[i]);
    }
    printf("\nCargando algun valor a las posiciones del vector\n");
    for (i=0;i<DIM;i++){
        vec[i]=i+1;
    }
    printf("Mostrando el contenido del vector\n");
    for (i=0;i<DIM;i++){
        printf("%d ",vec[i]);
    }
    printf("\n\n");
}

void f4_realloc(){
    // sirve para redimensionar el vector (creado en forma dinamica) a un tama�o mayor o menor
    // void *realloc(void *ptr, size_t nuevo_tama�o);
    int *vec,*vec2,*vec3,i,dimNueva;

    printf("Creamos el vector de %d posiciones\n",DIM);
    vec=(int*)calloc(DIM,sizeof(int)); // este espacio de memoria es CONTIGUO
    printf("Cargamos datos al vector\n");
    for (i=0;i<DIM;i++){
        vec[i]=i+1;
    }
    printf("Mostramos el vector de %d posiciones\n",DIM);
    for (i=0;i<DIM;i++){
        printf("%d-",vec[i]);
    }
    printf("\n");
    /* redimensionamos el vector */
    dimNueva=DIM-4;
    printf("Ahora la nueva dimension sera %d (menor a la anterior)\n",dimNueva);
    vec2=(int*)realloc(vec, dimNueva * sizeof(int)); // este espacio de memoria es CONTIGUO
    printf("Mostramos el nuevo vector de %d posiciones\n",dimNueva);
    for (i=0;i<dimNueva;i++){
        printf("%d-",vec2[i]);
    }
    printf("\n");
    dimNueva=dimNueva*2;
    printf("Ahora la nueva dimension sera %d (mayor a la anterior)\n",dimNueva);
    vec3=(int*)realloc(vec2, dimNueva * sizeof(int)); // este espacio de memoria es CONTIGUO
    for (i=0;i<dimNueva;i++){
        printf("%d-",vec2[i]);
    }
    printf("\n");
}

void f5_free(){
    // esta funcion libera el espacio de memoria pedido por las funciones anteriores
    // es muy importante usarlas siempre (no como en las funciones anteriores que no se uso nunca
    int *vec,i;

    printf("Pedimos un espacio de memoria de %d posiciones y le asignamos un 8 a cada posicion\n",DIM);
    vec=(int*)calloc(DIM,sizeof(int));
    for (i=0;i<DIM;i++){
        vec[i]=8;
    }
    printf("El vector:\n");
    for (i=0;i<DIM;i++){
        printf("%d-",vec[i]);
    }
    printf("\nLiberamos el espacio del vector\n");
    free(vec);
}

void f6_matrizDinamica(){
    int **mat,i,j;

    printf("\nCreando vector principal\n");
    mat=(int**)calloc(DIM,sizeof(int*));
    for(i=0;i<DIM;i++){
        printf("Creando fila %d de %d posiciones...\n",i,DIM);
        mat[i]=(int*)calloc(DIM,sizeof(int));
    }
    printf("\nCargando matriz con datos...\n");
    for(i=0;i<DIM;i++)
        for(j=0;j<DIM;j++)
            mat[i][j]=i+j;
    printf("\nMostrando matriz\n");
    for(i=0;i<DIM;i++){
        for(j=0;j<DIM;j++){
            printf("%d\t",mat[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

int main(){
    int opcion;
    FILE* unArchivo;

    banner();
    opcion=menu();
    while(opcion!=0){
        switch(opcion){
            case 0:
                return 0;
            case 1:
                f1_malloc();
                break;
            case 2:
                f2_malloc();
                break;
            case 3:
                f3_calloc();
                break;
            case 4:
                f4_realloc();
                break;
            case 5:
                f5_free();
                break;
            case 6:
                f6_matrizDinamica();
                break;
            default:
                errores(1);
                break;
        }
        opcion=menu();
    }
    return 0;
    }
